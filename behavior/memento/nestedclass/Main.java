package behavior.memento.nestedclass;

public class Main {

	public static void main(String[] args) {
		Editor editor = new Editor();
		EditorHistory history  = new EditorHistory();
		
		
		editor.appendContent("Hello");
		editor.appendContent(" World");
		history.saveSnapshot(editor);
		System.out.println(editor.getContent());
		
		
		editor.appendContent(" :) ");
		history.saveSnapshot(editor);
		System.out.println(editor.getContent());

		editor.appendContent("Hello Again");
		System.out.println(editor.getContent());
		

		history.restoreSnapshot(editor);
		System.out.println(editor.getContent());
		
		history.restoreSnapshot(editor);
		
		
		
		
		System.out.println(editor.getContent());
		
		
		
		
		

	}

}
