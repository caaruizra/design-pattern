package behavior.memento.nestedclass;

import java.util.Stack;

public class EditorHistory {
	
	Stack<Object> history;
	
	
	public EditorHistory(){
		history = new Stack<>();
	}
	
	public void saveSnapshot(Editor editor) {
		history.push(editor.createMemento());
		
	}
	
	public void restoreSnapshot(Editor editor) {
		Object snapshot = history.pop();
		editor.restoreFromMemento(snapshot);
	}

}
