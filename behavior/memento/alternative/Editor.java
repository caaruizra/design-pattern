package behavior.memento.alternative;

public class Editor {

	    private String content = "";

	    public void appendContent(String content) {
	        this.content += content;
	    }

	    public String getContent() {
	        return content;
	    }

	    public Memento createMemento() {
	        return new Memento(content);
	    }

	    public void restoreFromMemento(Object memento) {
	        content = ((Memento) memento).getContent();
	    }
	    
	    
	
	 
	    

}
