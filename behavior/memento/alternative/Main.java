package behavior.memento.alternative;

import java.util.Stack;

public class Main {

	public static void main(String[] args) {
		Editor editor = new Editor();
		Stack<IMemento> history = new Stack<>();
		
		
	
		
		
		editor.appendContent("Hello");
		editor.appendContent(" World");
		history.push(editor.createMemento());
		
		
		System.out.println(editor.getContent());
		
		
		editor.appendContent(" :) ");
		history.push(editor.createMemento());

		
		
		
		System.out.println(editor.getContent());

		editor.appendContent("Hello Again");
		System.out.println(editor.getContent());
		

		editor.restoreFromMemento(history.pop());
		System.out.println(editor.getContent());
		
		editor.restoreFromMemento(history.pop());
		System.out.println(editor.getContent());
		
		
		
		
		

	}

}
