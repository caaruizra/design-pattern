package behavior.memento.alternative;

public class Memento implements IMemento {
	   
    	String content;
    	
    	public String getContent() {
    		return this.content;
    	}
    	
    	public Memento(String content) {
    		this.content = content;
    	}

}
