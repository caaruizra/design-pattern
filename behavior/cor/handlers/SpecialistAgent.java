package behavior.cor.handlers;

import behavior.cor.Request;
import behavior.cor.RequestType;

public class SpecialistAgent implements Handler {

	private Handler nextHandler;

	@Override
	public void setNextHandler(Handler nextHandler) {
		this.nextHandler = nextHandler;
	}

	@Override
	public void handleRequest(Request request) {
		if (request.getType().equals(RequestType.HARD)) {
			System.out.println("Request handled by Specialist");
		} else if (nextHandler != null) {
			nextHandler.handleRequest(request);
		}
	}

}
