package behavior.cor.handlers;

import behavior.cor.Request;

public interface Handler {
	void setNextHandler(Handler nextHandler);
	void handleRequest(Request request);
}
