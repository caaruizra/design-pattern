package behavior.cor.handlers;

import behavior.cor.Request;
import behavior.cor.RequestType;

public class TechnicalSupportAgent implements Handler {

	private Handler nextHandler;

	@Override
	public void setNextHandler(Handler nextHandler) {
		this.nextHandler = nextHandler;
	}

	@Override
	public void handleRequest(Request request) {
		if (request.getType().equals(RequestType.MEDIUM)) {
			System.out.println("Request handled by technicalSupport");
		} else if (nextHandler != null) {
			nextHandler.handleRequest(request);
		}
	}

}
