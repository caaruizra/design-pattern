package behavior.cor.handlers;

import behavior.cor.Request;
import behavior.cor.RequestType;

public class HelpDesk implements Handler {

	private Handler nextHandler;

	@Override
	public void setNextHandler(Handler nextHandler) {
		this.nextHandler = nextHandler;
	}

	@Override
	public void handleRequest(Request request) {
		if (request.getType().equals(RequestType.EASY)) {
			System.out.println("Request handled by helpDesk");
		} else if (nextHandler != null) {
			nextHandler.handleRequest(request);
		}
	}

}
