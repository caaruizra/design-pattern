package behavior.cor;

import behavior.cor.handlers.ExternalAgent;
import behavior.cor.handlers.Handler;
import behavior.cor.handlers.HelpDesk;
import behavior.cor.handlers.SpecialistAgent;
import behavior.cor.handlers.TechnicalSupportAgent;

public class Main {
	
	public static void main(String[] args) {
		Handler helpDesk = new HelpDesk();
		Handler technicalSupport = new TechnicalSupportAgent();
		Handler specialist = new SpecialistAgent();
		Handler externalAgent = new ExternalAgent();
		
		
		helpDesk.setNextHandler(technicalSupport);
		technicalSupport.setNextHandler(specialist);
		specialist.setNextHandler(externalAgent);
		
		Request r1 = new Request(RequestType.EASY, "El computador no enciende porque se fue la electricidad del edificio");
		System.out.println(r1.getDescription());
		helpDesk.handleRequest(r1);
		
		Request r2 = new Request(RequestType.MEDIUM, "El computador muestra el mensaje NO HAY SISTEMA OPERATIVO INSTALDO");
		System.out.println(r2.getDescription());
		helpDesk.handleRequest(r2);
		
		Request r3 = new Request(RequestType.HARD, "El computador inicia pero el software contable dice que no hay movimientos contables");
		System.out.println(r3.getDescription());
		helpDesk.handleRequest(r3);
		
		Request r4 = new Request(RequestType.NOIDEA, "El computador inicia pero el software contable dice SU SISTEMA HA SIDO INFECTADO ENVIE 100 Criptomonedas para recuperar sus datos");
		System.out.println(r4.getDescription());
		helpDesk.handleRequest(r4);
		
	}


}
