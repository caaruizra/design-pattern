package behavior.templatemethod;

abstract class LoginTemplate {

	public String username;
	public String password;

	public final void doLogin() {
		System.out.println("Template method starts");

		if (checkInLdap()) {
			System.out.println("Checking in ldap ");
			if (!validateUserLdap()) {
				showErrorLogin();
				return;
			}
			System.out.println("User verified in ldap ");

		}

		if (!verifyLocalAcess()) {
			showErrorLogin();
			return;
		}

		sessionStart();
		redirect();
		System.out.println("Template method ends");
	}

	protected abstract boolean checkInLdap();

	protected boolean validateUserLdap() {
		return username.equals("user") && password.equals("abcd1234");
	}

	protected abstract boolean verifyLocalAcess();

	protected void sessionStart() {
		System.out.println("User is logged in, preparing cookie for " + username);

	};

	protected void redirect() {
		System.out.println("Redirect");
	};

	protected void showErrorLogin() {
		System.out.println("Nombre de usuario o contraseña incorrecto");
	};

}

class FreshmanLogin extends LoginTemplate {
	@Override
	protected boolean checkInLdap() {
		return false;
	}

	@Override
	protected boolean verifyLocalAcess() {
		return username.equalsIgnoreCase("pedro") || username.equalsIgnoreCase("juan");
	}

}

class StudentLogin extends LoginTemplate {
	@Override
	protected boolean checkInLdap() {
		return true;
	}

	@Override
	protected boolean verifyLocalAcess() {
		// Simula que hay un usuario de nombre User en la apliucación y que tiene acceso
		return username.equalsIgnoreCase("user");
	}

}

public class Main {
	public static void main(String[] args) {
		// Ejemplo con Primiparo

		System.out.println("..... Freshman ..........");
		LoginTemplate loginFreshman = new FreshmanLogin();
		loginFreshman.username = "ricardo";
		loginFreshman.doLogin();

		// Ejemplo con Student Login
		System.out.println("..... Student ..........");
		LoginTemplate studentLogin = new StudentLogin();
		studentLogin.username = "edwin";
		studentLogin.password = "abcd1234";
		studentLogin.doLogin();

	}
}
