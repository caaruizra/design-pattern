package behavior.state;

public class Main {

	public static void main(String[] args) {
		Document doc = new Document();
		doc.showState();
		
		doc.publish();
		doc.showState();
		
		doc._return();
		doc.showState();

		doc.publish();
		doc.showState();
		
		doc.publish();
		doc.showState();
		
		doc.publish();
		doc.showState();


	}

}
