package behavior.state.States;

import behavior.state.Document;

public class PublishedState implements IState {
	
	Document doc;
	
	public void setDoc(Document doc) {
		this.doc = doc;
	}

	@Override
	public void doPublish() {
		System.out.println("Un documento en estado publicado no puede ser publicado");
		
	}

	@Override
	public void doReject() {
			System.out.println("Un documento en estado publicado no puede ser rechazado");
		
	}

	@Override
	public void doReturn() {
		System.out.println("Un documento en estado publicado no puede ser devuelto");
		
	}
	
	@Override
	public void showState() {
		System.out.println("Estado: Publicado");
		
	}
	

}
