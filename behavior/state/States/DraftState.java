package behavior.state.States;

import behavior.state.Document;

public class DraftState implements IState {
	
	Document doc;
	
	public void setDoc(Document doc) {
		this.doc = doc;
	}

	@Override
	public void doPublish() {
		InReviewState state = new InReviewState();
		state.setDoc(doc);
		this.doc.setState(state);

	}

	@Override
	public void doReject() {
		RejectedState state = new RejectedState();
		state.setDoc(doc);
		this.doc.setState(state);

	}

	@Override
	public void doReturn() {
		System.out.println("Un documento en estado borrador no puede ser devuelto");
	}

	@Override
	public void showState() {
		System.out.println("Estado: Borrador");
		
	}
	

}
