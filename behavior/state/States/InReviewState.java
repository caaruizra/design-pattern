package behavior.state.States;

import behavior.state.Document;

public class InReviewState implements IState {

	Document doc;

	public void setDoc(Document doc) {
		this.doc = doc;
	}

	@Override
	public void doPublish() {
		PublishedState state = new PublishedState();
		state.setDoc(doc);
		this.doc.setState(state);

	}

	@Override
	public void doReject() {
		RejectedState state = new RejectedState();
		state.setDoc(doc);
		this.doc.setState(state);

	}

	@Override
	public void doReturn() {
		DraftState state = new DraftState();
		state.setDoc(doc);
		this.doc.setState(state);
	}
	
	@Override
	public void showState() {
		System.out.println("Estado: En Revisión");
		
	}

}
