package behavior.state.States;

public interface IState {
	public void doPublish();
	public void doReject();
	public void doReturn();
	
	public void showState();

}
