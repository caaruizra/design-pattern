package behavior.state;

import behavior.state.States.DraftState;
import behavior.state.States.IState;

public class Document {
	private IState state;
	
	public Document() {
		DraftState state = new DraftState();
		state.setDoc(this);
		this.state = state;
	}
	public void setState(IState state) {
		this.state = state;
	}
	
	public void reject() {
		state.doReject();
	}
	
	public void publish() {
		state.doPublish();
	}
	
	public void _return() {
		state.doReturn();
	}
	
	public void showState() {
		state.showState();
	}


}
