package behavior.iterator;

import java.util.Iterator;


import java.util.List;
import java.util.NoSuchElementException;
import java.util.Stack;

/**
 * Búsqueda DFS (Depth First Search)
 * 
 */
public class TreeDFSIterator<T> implements Iterator<T> {
	private Stack<ITreeNode<T>> stack;

	public TreeDFSIterator(ITreeNode<T> root) {
		this.stack = new Stack<>();
		this.stack.push(root);
	}

	@Override
	public boolean hasNext() {
		return !stack.isEmpty();
	}

	@Override
	public T next() {
		if (!hasNext()) {
			throw new NoSuchElementException("No more elements to iterate");
		}
		
		ITreeNode<T> node = stack.pop();
        
        List<TreeNode<T>> children = node.getChilds();
        for (int i = children.size() - 1; i >= 0; i--) {
            stack.push(children.get(i));
        }

		return node.getContent();
	}

}
