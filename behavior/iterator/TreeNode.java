package behavior.iterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TreeNode<T> implements ITreeNode<T> {
	
	T content;
	List<TreeNode<T>> childs;
	public TreeNode(T content) {
		this.content = content;
		this.childs = new ArrayList<>();
	}
	
	@Override
	public void addChild(TreeNode<T> child) {
		childs.add(child);
	}

	@Override
	public void deleteChild(TreeNode<T> child) {
		childs.remove(child);
	}
	
	@Override
	public T getContent() {
		return this.content;
	}
	
	@Override
	public void setContent(T content) {
		this.content=content;
	}
	
	@Override
	public List<TreeNode<T>> getChilds() {
		return childs;
	}

	@Override
	public Iterator<T> iterator() {
		return (Iterator<T>)  new TreeDFSIterator<T>(this);
	}
	
}
