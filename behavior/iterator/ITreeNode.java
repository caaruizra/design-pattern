package behavior.iterator;

import java.util.List;

public interface ITreeNode<T> extends Iterable<T> {

	List<TreeNode<T>> getChilds();

	void setContent(T content);

	T getContent();

	void deleteChild(TreeNode<T> child);

	void addChild(TreeNode<T> child);



}
