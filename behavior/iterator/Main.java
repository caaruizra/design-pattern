package behavior.iterator;

import java.util.Iterator;

public class Main {

	public static void main(String[] args) {
		TreeNode<String> root = new TreeNode<>("A");
		TreeNode<String> nodeB = new TreeNode<>("B");
		TreeNode<String> nodeC = new TreeNode<>("C");
		
		TreeNode<String> nodeD = new TreeNode<>("D");
		TreeNode<String> nodeE = new TreeNode<>("E");
		TreeNode<String> nodeF = new TreeNode<>("F");
		
		root.addChild(nodeB);
		root.addChild(nodeC);
	
		nodeC.addChild(nodeD);
		nodeC.addChild(nodeE);
		nodeC.addChild(nodeF);
		
		
		//Forma larga
		Iterator<String> iterator = root.iterator();
		while (iterator.hasNext()) {
			String data = iterator.next();
		    System.out.println(data);
		}
		
		//Forma corta
		for(String data : root) {
			System.out.println(data);
		}
		
		root.forEach( r-> System.out.println(r));
		
		//Forma corta
				
				
		

	}

}
