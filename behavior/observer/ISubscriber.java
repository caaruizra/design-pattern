package behavior.observer;

public interface ISubscriber {
	public void update(Object context);
}
