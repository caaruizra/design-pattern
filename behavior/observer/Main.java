package behavior.observer;

public class Main {
	public static void main(String[] args) {
		ConcreteSubscriber c1 = new ConcreteSubscriber();
		c1.setName("C1");
		ConcreteSubscriber c2 = new ConcreteSubscriber();
		c2.setName("C2");
		ConcreteSubscriber c3 = new ConcreteSubscriber();
		c3.setName("C3");
		Publisher p = new Publisher();
		p.setStatus("Initial");
		p.notifySubscribers();
		
		p.subscribe(c1);
		p.subscribe(c2);
		p.setStatus("Notifiado");
		p.notifySubscribers();
		
		
		
		
	}
}
