package behavior.observer;

import java.util.LinkedList;
import java.util.List;

public class Publisher {
	private List<ISubscriber> subscribers;
	private String status;
	
	public Publisher() {
		subscribers = new LinkedList<>();
	}
	
	public void setStatus(String status) { 
		this.status = status;
	}
	
	public void subscribe (ISubscriber s) {
		subscribers.add(s);
	}

	
	public void unsubscribe(ISubscriber s) {
		subscribers.remove(s);
	}
	
	public void notifySubscribers() {
		for(ISubscriber s: subscribers) {
			s.update(status);
		}
	}

}
