package behavior.observer;

public class ConcreteSubscriber implements ISubscriber{

	public String name;
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public void update(Object context) {
		System.out.println("Update received in "+name+" data "+context);
	}

}
