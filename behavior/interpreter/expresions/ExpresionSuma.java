package behavior.interpreter.expresions;

public class ExpresionSuma implements ISymbol {
    private ISymbol expresion1;
    private ISymbol expresion2;
    private String operador;

    public ExpresionSuma(ISymbol expresion1, ISymbol expresion2, String operador) {
        this.expresion1 = expresion1;
        this.expresion2 = expresion2;
        this.operador = operador;
    }

    public int interpretar() {
        switch (operador) {
            case "+":
                return expresion1.interpretar() + expresion2.interpretar();
            case "-":
                return expresion1.interpretar() - expresion2.interpretar();
            default:
                throw new UnsupportedOperationException("Operador de suma no válido.");
        }
    }
}