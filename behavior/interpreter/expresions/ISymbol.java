package behavior.interpreter.expresions;

public interface ISymbol {
    int interpretar();
}
