package behavior.interpreter.expresions;

public class ExpresionPrima implements ISymbol {
    private Object valor;

    public ExpresionPrima(Object valor) {
        this.valor = valor;
    }

    public int interpretar() {
        if (valor instanceof Integer) {
            return (int) valor;
        } else if (valor instanceof Expresion) {
            return ((ISymbol) valor).interpretar();
        }

        throw new UnsupportedOperationException("Expresión prima no válida.");
    }
}