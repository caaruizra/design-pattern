package behavior.interpreter;

import behavior.interpreter.expresions.Expresion;
import behavior.interpreter.expresions.ExpresionPrima;
import behavior.interpreter.expresions.ExpresionSuma;

public class Main {
	
	public static void main(String [] args) {
		
		//(2+3)*4
		ExpresionPrima exp1 = new ExpresionPrima(2);
		ExpresionPrima exp2 = new ExpresionPrima(3);
		ExpresionSuma suma = new ExpresionSuma(exp1, exp2, "+");
		ExpresionPrima exp3 = new ExpresionPrima(4);
		Expresion expresionFinal = new Expresion(suma, exp3, "*");

		int resultado = expresionFinal.interpretar();
		System.out.println(resultado); // Imprime 20
	
	}

}
