package behavior.visitor.problem.bad;

public class Exporter {
	public void export(Dot s) {
		System.out.println("Punto exportado x="+s.x+" y="+s.y);
	}
	
	
	public void export(Rectangle s) {
		System.out.println("Rectangulo exportado w="+s.width+" h="+s.height );
	}
	
	
	public void export(Circle c) {
		System.out.println("Círculo exportado radius="+c.radius );
	}

	
	public void export(Shape s) {
		System.out.println("Forma exportada "+s.id);
	}
	

}
