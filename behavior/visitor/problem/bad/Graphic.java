package behavior.visitor.problem.bad;

public interface Graphic {

	public void draw();
}
