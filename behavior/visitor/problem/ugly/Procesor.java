package behavior.visitor.problem.ugly;

import java.util.ArrayList;
import java.util.List;

public class Procesor {
	
	List<Shape> listOfShapes;

	public Procesor() {
		listOfShapes = new ArrayList<>();
	}
	
	public void exportAll() {
		Exporter e = new Exporter();

		for(Shape s : listOfShapes) {
			e.export(s);			
		}
		
	}
	
	public void addElement(Shape shape) {
		listOfShapes.add(shape);
	}

}
