package behavior.visitor.problem.ugly;

public class Exporter {
	
	public void export(Shape s) {
		if (s instanceof  Dot) {
			Dot d = (Dot) s;
			System.out.println("Punto exportado x="+d.x+" y="+d.y);
		}else if (s instanceof  Rectangle) {
			Rectangle r = (Rectangle) s;
			System.out.println("Rectangulo exportado w"+r.width+" h="+r.height );
		}else if (s instanceof  Shape) {
			System.out.println("Forma exportada "+s.id);
		}
	}
}
