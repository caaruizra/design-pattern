package behavior.visitor.problem.ugly;

public class Shape implements Graphic {
	public int id;
	
	@Override
	public void draw() {
		System.out.println("Printing shape with ID"+id);
	}

}
