package behavior.visitor.problem.ugly;

public interface Graphic {
	public void draw();
}
