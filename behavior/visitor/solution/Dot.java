package behavior.visitor.solution;

public class Dot extends Shape {
	public int x, y;
	
	
	@Override
	public void draw() {
		System.out.println("Drawin dot in x="+x+" y="+y);
	}
	
	@Override
	public void visit(IVisitor v) {
		v.doForDot(this);
	}

}
