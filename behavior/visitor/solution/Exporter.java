package behavior.visitor.solution;

public class Exporter implements IVisitor {

	@Override
	public void doForDot(Dot d) {
		System.out.println("Punto exportado x="+d.x+" y="+d.y);
		
	}

	@Override
	public void doForRectangle(Rectangle r) {
		System.out.println("Rectangulo exportado w"+r.width+" h="+r.height );

	}

	@Override
	public void doForShape(Shape s) {
		System.out.println("Forma exportada "+s.id);
		
	}

	@Override
	public void doForCircle(Circle c) {
		System.out.println("Círculo exportado radius="+c.radius );
	}
	
}
