package behavior.visitor.solution;

public interface Graphic {
	public void visit(IVisitor v);
	public void draw();
}
