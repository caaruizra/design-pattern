package behavior.visitor.solution;

public class Circle extends Shape {
	public int radius;
	
	
	@Override
	public void draw() {
		System.out.println("Drawin circle radius ="+radius);
	}

	@Override
	public void visit(IVisitor v) {
		v.doForCircle(this);
	}
}
