package behavior.visitor.solution;

public interface IVisitor {
	public void doForDot(Dot d);
	public void doForRectangle(Rectangle r);
	public void doForShape(Shape s);
	public void doForCircle(Circle c);

}
