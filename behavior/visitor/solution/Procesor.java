package behavior.visitor.solution;

import java.util.ArrayList;
import java.util.List;

public class Procesor {
	
	List<Shape> listOfShapes;

	public Procesor() {
		listOfShapes = new ArrayList<>();
	}
	
	public void exportAll() {
		Exporter e = new Exporter();

		for(Shape s : listOfShapes) {
			s.visit(e);			
		}
		
	}
	
	public void addElement(Shape shape) {
		listOfShapes.add(shape);
	}

}
