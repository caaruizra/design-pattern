package behavior.visitor.solution;

public class Shape implements Graphic {
	public int id;
	
	@Override
	public void draw() {
		System.out.println("Printing shape with ID"+id);
	}

	@Override
	public void visit(IVisitor v) {
		v.doForShape(this);
	}

}
