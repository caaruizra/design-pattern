package behavior.visitor.solution;

public class Rectangle extends Shape {
	public int width, height;
	
	
	@Override
	public void draw() {
		System.out.println("Drawin dot in w="+width+" h="+height);
	}

	@Override
	public void visit(IVisitor v) {
		v.doForRectangle(this);
	}
}
