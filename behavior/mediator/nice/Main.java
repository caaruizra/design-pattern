package behavior.mediator.nice;

import behavior.mediator.nice.Colleagues.Button;
import behavior.mediator.nice.Colleagues.Led;
import behavior.mediator.nice.Colleagues.PowerSupplier;
import behavior.mediator.nice.mediator.Dashboard;
import behavior.mediator.nice.mediator.IMediator;

public class Main {
	public static void main(String[] args) {
		
		Dashboard dashboard = new Dashboard();
		
		PowerSupplier powerSupply = new PowerSupplier(dashboard);
		Led ledLight = new Led(dashboard);
		Button button = new Button(dashboard);
		
		dashboard.setButton(button);
		dashboard.setLed(ledLight);
		dashboard.setPowerSupply(powerSupply);
		
		
		System.out.println("Before turn on .....");
		
		ledLight.printStatus();
		powerSupply.printStatus();
		button.press();
		

		System.out.println("After button pressed  .....");
		ledLight.printStatus();
		powerSupply.printStatus();
		
		
		System.out.println("After button pressed again .....");
		button.press();
		ledLight.printStatus();
		powerSupply.printStatus();
		
		
		
		
	}
}
