package behavior.mediator.nice.Colleagues;

import behavior.mediator.nice.mediator.IMediator;

public class Led extends Colleague{
	    
	    private boolean isOn = false;

	    public Led(IMediator imadiator) {
	    	super(imadiator);
		}
	    
	    public void printStatus() {
	    	if(isOn) {
				System.out.println("The led is on");
			}else {
				System.out.println("The led is off");
			}
	    }
	    
	    public boolean isOn() {
	    	return isOn;
	    }

	    public void turnOn() {
	        mediator.notify(this, "Led is on");
	        isOn = true;
	    }

	    public void turnOff() {
	        mediator.notify(this, "Led is off");
	        isOn = false;
	    }

		@Override
		public void receive(String message) {
			System.out.println("Led receive message... "+message);
			
		}

}
