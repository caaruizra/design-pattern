package behavior.mediator.nice.Colleagues;

import behavior.mediator.nice.mediator.IMediator;

public class Button extends Colleague {
	    
	    
	    public Button(IMediator mediator) {
	    	super(mediator);
	    }
	    
	    
		public void press(){
	    	mediator.notify(this,"button pressed");
	    }
		
		
		@Override
		public void receive(String message) {
	        System.out.println("Button received message: " + message);
		}
}
