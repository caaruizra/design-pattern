package behavior.mediator.nice.Colleagues;

import behavior.mediator.nice.mediator.IMediator;

public class PowerSupplier extends Colleague {
	
	public PowerSupplier(IMediator mediator) {
		super(mediator);
	}

	boolean isTurnedOn = false;
	
	
	public void printStatus() {
		if(isTurnedOn) {
			System.out.println("The power supply is turned on");
		}else {
			System.out.println("The power supply is turned off");
		}
		
	}
	 public void turnOn() {
		 	this.isTurnedOn =true;
	        mediator.notify(this, "Power Supply turned on");
	 }

	    public void turnOff() {
	        this.isTurnedOn=false;
	        mediator.notify(this, "Power Supply turned off");

	    }
		@Override
		public void receive(String message) {
			System.out.println("Power supply receive ... "+message);
			
		}

}
