package behavior.mediator.nice.Colleagues;

import behavior.mediator.nice.mediator.IMediator;

public abstract class Colleague {
    protected IMediator mediator;

    public Colleague(IMediator mediator) {
        this.mediator = mediator;
    }

    public abstract void receive(String message);
}