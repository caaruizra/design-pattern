package behavior.mediator.nice.mediator;

import behavior.mediator.nice.Colleagues.Colleague;

public interface IMediator {
	    void notify(Colleague colleague, String message);


}
