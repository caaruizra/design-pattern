package behavior.mediator.nice.mediator;

import behavior.mediator.nice.Colleagues.Button;
import behavior.mediator.nice.Colleagues.Colleague;
import behavior.mediator.nice.Colleagues.Led;
import behavior.mediator.nice.Colleagues.PowerSupplier;

public class Dashboard implements IMediator {

	private Button button;
	private Led led;
	private PowerSupplier powerSupply;

	public Button getButton() {
		return button;
	}

	public void setButton(Button button) {
		this.button = button;
	}

	public Led getLed() {
		return led;
	}

	public void setLed(Led led) {
		this.led = led;
	}

	public PowerSupplier getPowerSupply() {
		return powerSupply;
	}

	public void setPowerSupply(PowerSupplier powerSupply) {
		this.powerSupply = powerSupply;
	}

	@Override
	public void notify(Colleague colleague, String message) {
		System.out.println("message: " + message);
		if (colleague instanceof Button) {
			if (led.isOn()) {
				led.turnOff();
				powerSupply.turnOff();
			} else {
				led.turnOn();
				powerSupply.turnOn();
			}
		}

	}

}
