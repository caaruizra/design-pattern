package behavior.mediator.bad;

public class Button {
	    private Led led;
		public Button(Led led) {
			this.led = led;
		}
	    public void press(){
	        if(led.isOn()){
	            led.turnOff();
	        } else {
	            led.turnOn();
	        }
	    }
}
