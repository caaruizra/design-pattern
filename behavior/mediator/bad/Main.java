package behavior.mediator.bad;

public class Main {
	public static void main(String[] args) {
		PowerSupplier powerSupply = new PowerSupplier();
		Led ledLight = new Led(powerSupply);
		Button button = new Button(ledLight);
		
		System.out.println("Before turn on .....");
		
		ledLight.printStatus();
		powerSupply.printStatus();
		
		button.press();
		
		System.out.println("After button pressed  .....");
		
		ledLight.printStatus();
		powerSupply.printStatus();
		
		
		System.out.println("After button pressed again .....");
		button.press();
		ledLight.printStatus();
		powerSupply.printStatus();
		
		
		
		
	}
}
