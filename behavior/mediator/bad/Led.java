package behavior.mediator.bad;

public class Led {
	    private PowerSupplier powerSupplier;
	    private boolean isOn = false;

	    public Led(PowerSupplier powerSupply) {
	    	
			this.powerSupplier = powerSupply;
		}
	    
	    public void printStatus() {
	    	if(isOn) {
				System.out.println("The led is on");
			}else {
				System.out.println("The led is off");
			}
	    }
	    
	    public boolean isOn() {
	    	return isOn;
	    }

	    public void turnOn() {
	        powerSupplier.turnOn();
	        isOn = true;
	    }

	    public void turnOff() {
	        isOn = false;
	        powerSupplier.turnOff();
	    }

}
