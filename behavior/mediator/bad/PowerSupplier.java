package behavior.mediator.bad;

public class PowerSupplier {
	boolean isTurnedOn = false;
	
	
	public void printStatus() {
		if(isTurnedOn) {
			System.out.println("The power supply is turned on");
		}else {
			System.out.println("The power supply is turned off");
		}
		
	}
	 public void turnOn() {
	        this.isTurnedOn=true;
	    }

	    public void turnOff() {
	        this.isTurnedOn=false;
	    }

}
