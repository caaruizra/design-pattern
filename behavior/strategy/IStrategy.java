package behavior.strategy;

interface IStrategy {
    int doOperation(int num1, int num2);
}
