package behavior.strategy;

public class Main {
    public static void main(String[] args) {
        IStrategy addStrategy = new OperationAdd();
        IStrategy subtractStrategy = new OperationSubtract();
        IStrategy multiplyStrategy = new OperationMultiply();

        Context context = new Context(addStrategy);

        int result = context.executeStrategy(5, 3);
        System.out.println("Resultado: " + result);

        context = new Context(subtractStrategy);
        result = context.executeStrategy(5, 3);
        System.out.println("Resultado: " + result);
        
        context = new Context(multiplyStrategy);
        result = context.executeStrategy(5, 3);
        System.out.println("Resultado: " + result);
        
    }
}