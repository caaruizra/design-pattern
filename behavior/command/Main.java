package behavior.command;

public class Main {

	public static void main(String[] args) {

		Light light = new Light();

		Command lightOnCommand = new LightOnCommand(light);
		Command lightOffCommand = new LightOffCommand(light);

		RemoteControl remoteControl = new RemoteControl();
		remoteControl.setCommandA(lightOnCommand);
		remoteControl.pressButtonA(); 

		remoteControl.setCommandB(lightOffCommand);
		remoteControl.pressButtonB();

	}

}
