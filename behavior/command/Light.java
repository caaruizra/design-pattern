package behavior.command;

public class Light {
	boolean ligthOn = false;
	public void turnOn() {
        System.out.println("Light is on");
        ligthOn = true;
    }

    public void turnOff() {
        System.out.println("Light is off");
        ligthOn = false;
    }
    
    public boolean isLightOn() {
    	return ligthOn;
    }
}
