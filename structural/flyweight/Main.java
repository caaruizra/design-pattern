package structural.flyweight;

import structural.flyweight.trees.TreeFactory;

public class Main {

	public static void main(String[] args) {
		Forest forest = new Forest();
		TreeFactory treeFactory = new TreeFactory();
		
		String [] treeTextures = {"Cypress","Maple","Rosewood","Mahogany"};
		
		
		for(int i = 1; i < 10; i++) {
			for(int j = 1; j < 10; j++) {
				int randomTextureIndex = (int) Math.floor(Math.random()*3.9);
				forest.plantTree(treeFactory.createTree(i, j, "TreeTexture_"+treeTextures[randomTextureIndex]));
			}
		}
        
        forest.draw();
        System.out.println("KB: " + (double) (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024);

	}

}
