package structural.flyweight.trees;

import java.util.HashMap;

public class TreeFactory {
	HashMap<String,TreeTexture> textures;
	
	public TreeFactory() {
		this.textures = new HashMap<>();
	}
	
	public Tree createTree(int x, int y, String textureName) {
		if(!this.textures.containsKey(textureName)) {
			this.textures.put(textureName, new TreeTexture(textureName));
		}
		return new Tree(x, y, textures.get(textureName));
	}
	
	/* Esto sería para mostrar que ocurriría 
	 public Tree createTree(int x, int y, String textureName) {
		return new Tree(x, y, new TreeTexture(textureName));
	}
	 */

}
