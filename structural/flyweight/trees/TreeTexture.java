package structural.flyweight.trees;


public class TreeTexture {
	String texture;
	Byte [] textureData;

	public TreeTexture(String texture) {
        this.texture = texture;
        this.loadTextureInMemory();
	}
	
	private void loadTextureInMemory() {
		System.out.println("Textura "+texture+" cargada");
		this.textureData = new Byte[1024*1024];
	}
	
	public void draw(int x, int y) {
		System.out.println("Drawing Tree "+x+","+y+" texture: "+texture);
	}
	

}
