package structural.flyweight.trees;

public class Tree {
	TreeTexture texture;

	private int x;
	private int y;

	public Tree(int x, int y, TreeTexture texture) {
		this.x = x;
		this.y = y;
		this.texture = texture;
	}

	public void draw() {
		texture.draw(x, y);
	}

}
