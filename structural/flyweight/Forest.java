package structural.flyweight;

import java.util.LinkedList;
import java.util.List;


import structural.flyweight.trees.Tree;

public class Forest {
	List<Tree> trees;
	
	public Forest() {
		this.trees = new LinkedList<>();
	}
	
	public void plantTree(Tree tree) {
		this.trees.add(tree);
		
	}
	public void draw() {
		for(Tree tree: trees) {
			tree.draw();
		}
	}

}
