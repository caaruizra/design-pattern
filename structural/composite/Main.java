package structural.composite;

public class Main {

	public static void main(String[] args) {
		
		OrganizationMember itm = new OrganizationUnit("ITM");
		OrganizationMember itmFraternidad = new OrganizationUnit("ITM Fraternidad");
		OrganizationMember itmRobledo = new OrganizationUnit("ITM Robledo");
		// Fraternidad
		
		OrganizationMember registroFraternidad = new InstitutionalAccount("Registro", "H-5");
		OrganizationMember alejandroUser = new PersonalAccount("Alejandro", "Ruiz");
		OrganizationMember juanUser = new PersonalAccount("Juan", "Perez");

		// Robledo 
		OrganizationMember registroRobledo = new InstitutionalAccount("Registro Robledo" , "M-50");
		OrganizationMember mjUser = new PersonalAccount("Michael", "Jackson");
		OrganizationMember rsUser = new PersonalAccount("Robert", "Smith");
		
		
		itm.add(itmFraternidad);
		itm.add(itmRobledo);
		
		itmFraternidad.add(alejandroUser);
		itmFraternidad.add(registroFraternidad);
		itmFraternidad.add(juanUser);
		
		itmRobledo.add(registroRobledo);
		itmRobledo.add(mjUser);
		itmRobledo.add(rsUser);


		itm.showInfo();
		
		
	}

}
