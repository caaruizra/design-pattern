package structural.composite;

//Component
public abstract class OrganizationMember {
	abstract public void add(OrganizationMember componente);
	abstract public void delete(OrganizationMember componente);
	abstract public void showInfo(); 
}
