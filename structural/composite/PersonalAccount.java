package structural.composite;

public class PersonalAccount extends OrganizationMember {
    private String name;
    private String lastName;

    public PersonalAccount(String name, String lastName) {
        this.name = name;
        this.lastName = lastName;
    }
	@Override
	public void add(OrganizationMember componente) {
		// No implementado
	}

	@Override
	public void delete(OrganizationMember componente) {
		// No implementado

	}

	@Override
	public void showInfo() {
        System.out.println("Personal Account: " + name+" Lastname: "+lastName);
	}

}
