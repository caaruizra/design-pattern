package structural.composite;

import java.util.ArrayList;
import java.util.List;

public class OrganizationUnit extends OrganizationMember {
	 private String name;
	    private List<OrganizationMember> childs;

	    public OrganizationUnit(String nombre) {
	        this.name = nombre;
	        this.childs = new ArrayList<>();
	    }

	    @Override
	    public void showInfo() {
	        System.out.println("Organización: " + name);
	        System.out.println("Componentes:");
	        for (OrganizationMember componente : childs) {
	            componente.showInfo();
	        }
	    }

		@Override
		public void add(OrganizationMember componente) {
	        childs.add(componente);

		}

		@Override
		public void delete(OrganizationMember componente) {
	        childs.remove(componente);
		}
}
