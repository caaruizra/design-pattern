package structural.composite;

public class InstitutionalAccount extends OrganizationMember {
	private String name;
	private String location;

	public InstitutionalAccount(String name, String location) {
		this.name = name;
		this.location = location;
	}

	@Override
	public void add(OrganizationMember componente) {
		// No implementado
	}

	@Override
	public void delete(OrganizationMember componente) {
		// No implementado

	}

	@Override
	public void showInfo() {
		System.out.println("Institutional account: " + name + " Lastname: " + location);
	}

}
