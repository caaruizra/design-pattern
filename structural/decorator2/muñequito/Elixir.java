package structural.decorator2.muñequito;

public class Elixir extends DecoradorMuñequitos {

	public Elixir(IMuñequito muñequito) {
		super(muñequito);
	}
	
	@Override
	public int calularResistencia() {
		return muñequito.calularResistencia() + 2;
	}
	
	

}
