package structural.decorator2.muñequito;

public interface IMuñequito {
	int calularResistencia();
	void recibirGolpe();
}
