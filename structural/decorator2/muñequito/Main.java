package structural.decorator2.muñequito;

public class Main {

	public static void main(String[] args) {
		
		IMuñequito muñeco = new Muñequito();
		System.out.println("Resistencia: " + muñeco.calularResistencia());
		
		
		muñeco = new Escudo(muñeco);
		System.out.println("Resistencia: " + muñeco.calularResistencia());

		muñeco = new Elixir(muñeco);
		System.out.println("Resistencia: " + muñeco.calularResistencia());

		muñeco = new Elixir(muñeco);
		System.out.println("Resistencia: " + muñeco.calularResistencia());
		
		muñeco = new Escudo(muñeco);
		System.out.println("Resistencia: " + muñeco.calularResistencia());

		

	}

}
