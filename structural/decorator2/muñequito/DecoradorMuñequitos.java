package structural.decorator2.muñequito;

public abstract class DecoradorMuñequitos implements IMuñequito {
	IMuñequito muñequito;
	
	public DecoradorMuñequitos(IMuñequito muñequito) {
		this.muñequito = muñequito;
	}
	
	@Override
	public int calularResistencia() {
		return muñequito.calularResistencia();
		
	}

	@Override
	public void recibirGolpe() {
		muñequito.recibirGolpe();
	}

}
