package structural.decorator2.muñequito;

public class Muñequito implements IMuñequito {
	int salud = 100;
	int resistencia = 1;
	
	@Override
	public int calularResistencia() {
		return resistencia;
	}
	
	public void recibirGolpe() {
		salud -= 10;
	}

}
