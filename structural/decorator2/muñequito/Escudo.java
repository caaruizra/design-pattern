package structural.decorator2.muñequito;

public class Escudo extends DecoradorMuñequitos {

	public Escudo(IMuñequito muñequito) {
		super(muñequito);
	}
	
	@Override
	public int calularResistencia() {
		return muñequito.calularResistencia() + 5;
	}

}
