package structural.decorator;

public class EmailMessage implements IMessage {
	private String content;

	public EmailMessage(String content) {
		this.content = content;
	}

	@Override
	public void send() {
		System.out.println("Sending email: " + content);
	}
}
