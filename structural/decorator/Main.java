package structural.decorator;

public class Main {

	public static void main(String[] args) {
		  IMessage mail1 = new EmailMessage("Hello, world!");
	      IMessage mailWithSignature = new MessageWithSignature(mail1, "-- The best developer in the world");
	      mailWithSignature.send();
	      
	      
	      IMessage mail2 = new EmailMessage("Hello, world!");
	      IMessage mailWithSignature2 = new MessageWithSignature(mail2, "-- The best developer in the world");
	      IMessage mailWithSignatureAndEncryption = new MessageWithEncryption(mailWithSignature2);
	      
	      mailWithSignatureAndEncryption.send();
	      
	}

}
