package structural.decorator;

public class MessageWithSignature extends MessageDecorator  {
	    private String signature;

	    public MessageWithSignature(IMessage decoratedMessage, String signature) {
	        super(decoratedMessage);
	        this.signature = signature;
	    }

	    @Override
	    public void send() {
	        super.send();
	        addSignature();
	    }

	    private void addSignature() {
	        System.out.println("Added signature: " + signature);
	    }
	
}
