package structural.decorator;

public interface IMessage {
	    void send();
}
