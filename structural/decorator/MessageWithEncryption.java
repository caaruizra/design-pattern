package structural.decorator;

public class MessageWithEncryption extends MessageDecorator {
	public MessageWithEncryption(IMessage decoratedMessage) {
		super(decoratedMessage);
	}

	@Override
	public void send() {
		encryptMessage();
		super.send();
	}

	private void encryptMessage() {
		System.out.println("Encrypting message...");
	}

}
