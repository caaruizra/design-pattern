package structural.decorator;

public abstract class MessageDecorator implements IMessage {
    protected IMessage decoratedMessage;

    public MessageDecorator(IMessage decoratedMessage) {
        this.decoratedMessage = decoratedMessage;
    }

    @Override
    public void send() {
        decoratedMessage.send();
    }
}