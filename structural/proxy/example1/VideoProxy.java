package structural.proxy.example1;

public class VideoProxy implements IVideo {
    private String videoUrl;
    private ConcreteVideo video;

    public VideoProxy(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public void play() {
        if (video == null) {
            video = new ConcreteVideo(videoUrl);
        }
        video.play();
    }
}