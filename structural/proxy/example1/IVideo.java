package structural.proxy.example1;

public interface IVideo {
    void play();
}
