package structural.proxy.example1;

public class Main {

	public static void main(String[] args) {
		IVideo video = new VideoProxy("c://videos/video.mp4");
		// The video is not loaded yet
		video.play();

		video.play();
	}

}
