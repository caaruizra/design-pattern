package structural.proxy.example2;

public class Main {

	public static void main(String[] args) {
		Video video = new VideoProxy("c://videos/video.mp4");
		video.play();

		Video video2 = new VideoProxy("c://videos/video2.mp4");
		video2.play();
	}

}
