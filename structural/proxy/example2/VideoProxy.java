package structural.proxy.example2;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class VideoProxy implements Video {
	private String videoUrl;
    private ConcreteVideo video;
    private List<String> playedVideoHistory;

    public VideoProxy(String videoUrl) {
        this.videoUrl = videoUrl;
        playedVideoHistory = new ArrayList<>();
    }

    public void play() {
        if (video == null) {
            video = new ConcreteVideo(videoUrl);
        }
        video.play();
        saveToHistory();
    }

    private void saveToHistory() {
        playedVideoHistory.add(videoUrl);
        try (PrintWriter writer = new PrintWriter(new FileWriter("video_history.txt", true))) {
            writer.println(videoUrl);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}