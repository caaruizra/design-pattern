package structural.proxy.example2;

public interface Video {
    void play();
}
