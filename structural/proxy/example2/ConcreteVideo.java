package structural.proxy.example2;

import java.util.concurrent.TimeUnit;

public class ConcreteVideo implements Video {
    private String videoUrl;

    public ConcreteVideo(String videoUrl) {
        this.videoUrl = videoUrl;
        loadFromDisk(); 
    }

    private void loadFromDisk() {
        System.out.println("Loading video from disk: " + videoUrl);
        try {
			TimeUnit.SECONDS.sleep(3);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

    }

    public void play() {
        System.out.println("Playing video: " + videoUrl);
    }
}
