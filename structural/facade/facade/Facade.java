package structural.facade.facade;

import structural.facade.verycomplexapp.CMSSystem;
import structural.facade.verycomplexapp.SignatureSystem;
import structural.facade.verycomplexapp.TransformerSystem;

public class Facade {
	public void signDocument(String fileContent, String fileName, String [] positions) {
		CMSSystem cms = new CMSSystem();
		cms.createDocument(fileContent, fileName);
		cms.checkoutDocument(fileName);
		
		if(fileName.toLowerCase().endsWith(".docx")) {
			TransformerSystem ts = new TransformerSystem();
			fileName = ts.convertWord2PDF(fileName);
		}else if(fileName.toLowerCase().endsWith(".xlsx")){
			TransformerSystem ts = new TransformerSystem();
			fileName = ts.convertExel2PDF(fileName);
		}
		
		SignatureSystem ss = new SignatureSystem();
		for(int i = 0; i < positions.length;i++) {
			ss.addSignature(fileName, positions[i] );	
		}
		ss.signDocument(fileName);
		cms.checkinDocument(fileName);
	}
}
