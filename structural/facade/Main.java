package structural.facade;

import structural.facade.facade.Facade;

public class Main {
	public static void main(String [] args) {
		Facade system = new Facade();
		
		String content = "Hello World";
		String fileName = "hello.docx";
		String [] positions = {"emplyee","manager"};
		system.signDocument(
				content, 
				fileName,
				positions
			);
	}

}
