package structural.facade.verycomplexapp;

public class TransformerSystem {
	public String convertWord2PDF(String file) {
		System.out.println("New file created: "+file.replace(".docx", ".pdf"));
		return file.replace(".docx", ".pdf");
	}

	public String convertExel2PDF(String file) {
		System.out.println("New file created: "+file.replace(".xlsx", ".pdf"));
		return file.replace(".xlsx", ".pdf");
	}
	
	
	
}
