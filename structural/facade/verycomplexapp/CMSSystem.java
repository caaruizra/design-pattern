package structural.facade.verycomplexapp;

public class CMSSystem {
	public void checkinDocument(String file) {
		System.out.println("Chek in document: "+file);
	}

	public void checkoutDocument(String file) {
		System.out.println("Cheked out document: "+file);
	}
	
	public void createDocument(String file, String fileContent) {
		System.out.println("Created document: "+file);
	}
	
}
