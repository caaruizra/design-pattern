package structural.facade.verycomplexapp;

public class SignatureSystem {
	public void addSignature(String file, String position) {
		System.out.println("Added signature "+position+" to "+file);
	}
	
	public void changeDocumentStatus(String file, String status) {
		System.out.println("Changed status "+status+" to "+file);
	}
	
	public void signDocument(String file) {
		System.out.println("Document is signed "+file);
	}
	

}
