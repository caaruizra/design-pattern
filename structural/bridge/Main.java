package structural.bridge;

import structural.bridge.implementor.*;
import structural.bridge.abstraction.*;
public class Main {
    public static void main(String[] args) {
        GamePlatform xbox = new Xbox();
        GamePlatform playStation = new PlayStation();

        VideoGame adventureGameXbox = new AdventureGame(xbox);
        VideoGame sportsGamePlayStation = new SportsGame(playStation);

        adventureGameXbox.start();
        adventureGameXbox.close();

        sportsGamePlayStation.start();
        sportsGamePlayStation.close();
    }
}