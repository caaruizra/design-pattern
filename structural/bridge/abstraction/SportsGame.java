package structural.bridge.abstraction;

import structural.bridge.implementor.GamePlatform;

//Refined Abstraction

public class SportsGame extends VideoGame {

	public SportsGame(GamePlatform platform) {
		super(platform);
	}

	@Override
	public void start() {
		System.out.println("Starting sports game.");
		platform.play();
		platform.drawLine();
		platform.drawLine();
		platform.drawLine();

	}

	@Override
	public void close() {
		System.out.println("Closing sports game.");
	}

}
