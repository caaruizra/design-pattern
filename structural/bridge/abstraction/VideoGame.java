package structural.bridge.abstraction;

import structural.bridge.implementor.GamePlatform;

public abstract class VideoGame {
    protected GamePlatform platform;

    public VideoGame(GamePlatform platform) {
        this.platform = platform;
    }

    public abstract void start();
    public abstract void close();
}