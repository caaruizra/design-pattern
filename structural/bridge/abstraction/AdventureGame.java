package structural.bridge.abstraction;

import structural.bridge.implementor.GamePlatform;

//Refined Abstraction

public class AdventureGame extends VideoGame {

 public AdventureGame(GamePlatform platform) {
		super(platform);
	}

@Override
 public void start() {
     System.out.println("Starting adventure game.");
     platform.play();
     platform.drawLine();
     platform.drawRectangle();
 }

 @Override
 public void close() {
     System.out.println("Closing adventure game.");
 }
}