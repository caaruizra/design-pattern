package structural.bridge.implementor;

public class PlayStation implements GamePlatform {

	@Override
	public void play() {
        System.out.println("Playing on PlayStation..");
	}

	@Override
	public void drawRectangle() {
        System.out.println("A very complicated draw of rectangle in PlayStation...");

		
	}

	@Override
	public void drawLine() {
        System.out.println("A very complicated draw of line in PlayStation...");
		
	}

}
