package structural.bridge.implementor;

public class Xbox implements GamePlatform {

	@Override
	public void play() {
        System.out.println("Playing on Xbox.");
	}

	@Override
	public void drawRectangle() {
        System.out.println("A very complicated draw of rectangle in Xbox...");
		
	}

	@Override
	public void drawLine() {
        System.out.println("A very complicated draw of line in Xbox...");
		
	}

}
