package structural.bridge.implementor;

public interface GamePlatform {
	void play();
	void drawRectangle();
	void drawLine();
}
