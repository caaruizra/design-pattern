package structural.adapter;

import structural.adapter.adapters.USBReader;
import structural.adapter.adapters.USBToRS232Adapter;
import structural.adapter.existingApp.MedicalSamples;

public class Client {

	public static void main(String[] args) {
		USBReader usbReader = new USBReader("100xF1050");
		USBToRS232Adapter adapter = new USBToRS232Adapter(usbReader);
		MedicalSamples medicalSamplesProcessor = new MedicalSamples();
		medicalSamplesProcessor.process(adapter);
	}

}
