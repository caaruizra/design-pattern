package structural.adapter.existingApp;

public interface RS232SerialReader {
	public String connect(String comPort, long baudrate);
	public String getData();
	public void putData(String data);
}
