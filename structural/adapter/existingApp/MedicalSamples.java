package structural.adapter.existingApp;

public class MedicalSamples {
	public void process(RS232SerialReader reader) { 
		reader.connect("com1", 9600);
		reader.putData("1050");
		System.out.println(reader.getData());
	}
}
