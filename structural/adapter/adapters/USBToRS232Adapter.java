package structural.adapter.adapters;

import structural.adapter.existingApp.RS232SerialReader;

public class USBToRS232Adapter implements RS232SerialReader {
	USBReader usbReader;
	
	
	public USBToRS232Adapter(USBReader usbReader) {
		this.usbReader = usbReader;
	}
	
	@Override
	public String getData() {
		String response = "Reading device "+this.usbReader.deviceId+"\n";
		for(int i = 0; i < 10; i++) 
			response += "Result"  + usbReader.getMedicalSample(Integer.toString(i))+"\n";
		
		return response;
	}

	@Override
	public String connect(String comPort, long baudrate) {
		return "Ok";
	}

	@Override
	public void putData(String data) {
		usbReader.setDeviceId(data);
	}

}
