package structural.adapter.adapters;

public class USBReader {
	
	String deviceId;
	
	public USBReader(String deviceId) {
		this.deviceId = deviceId;
	}
	
	public void connectDevice() {
		System.out.println("The device "+deviceId+" is connected"); 
	}
	
	
	public String getMedicalSample(String slot) {
		return "slot "+slot+": result "+(Math.random()*1000);
	}
	
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

}
