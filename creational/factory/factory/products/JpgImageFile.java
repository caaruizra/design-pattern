package factory.factory.products;

public class JpgImageFile implements ImageFile {

	@Override
	public void openImage() {
		System.out.println("Imagen se muestra archivo JPG");
	}

	@Override
	public void resize(int width, int height) {
		System.out.println("Imagen se redimensiona a "+width+"X"+ height);
	}

}
