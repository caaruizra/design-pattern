package factory.factory;


public class Main {
	
	//Se mantiene la instanacia de un creador. 
	static ImageHandler handler;
	
	// De acuerdo a la configuración se instancia la clase creadora
	public static void configure(String ext){
		if(ext.equalsIgnoreCase("jpg")) {
			handler = new JpgImageHandler();
		}else {
			handler = new PngImageHandler();
		}
	}
	
	// Programa principal que procesa dos tipos de imagenes diferentes
	public static void main(String argsv[]) {
		 configure("jpg");
		 handler.processImage();
		 
		 configure("png");
		 handler.processImage();

		
	}

}
