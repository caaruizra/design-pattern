package factory.factory;

import factory.factory.products.ImageFile;
import factory.factory.products.PngImageFile;

public class PngImageHandler extends ImageHandler {

	@Override
	public ImageFile createImage() {
		return new PngImageFile();
	}

}
