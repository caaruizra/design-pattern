package factory.factory;

import factory.factory.products.ImageFile;

public abstract class ImageHandler {
	
	public abstract ImageFile createImage() ;
	
	public void processImage() {
		ImageFile image = this.createImage();
		image.openImage();
		image.resize(200, 300);
	}

}
