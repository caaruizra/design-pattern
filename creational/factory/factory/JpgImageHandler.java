package factory.factory;

import factory.factory.products.ImageFile;
import factory.factory.products.JpgImageFile;

public class JpgImageHandler extends ImageHandler {

	@Override
	public ImageFile createImage() {
		return new JpgImageFile();
	}

}
