package abstractFactory.products.concretos.victoriana;

import abstractFactory.products.Mesa;

public class MesaVictoriana extends Mesa {
	@Override
	public void plegar() {
			System.out.println("Destruyendo una mesa victoriana");		
	}
}
