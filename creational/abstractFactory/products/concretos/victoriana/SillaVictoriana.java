package abstractFactory.products.concretos.victoriana;

import abstractFactory.products.Silla;

public class SillaVictoriana extends Silla {

	@Override
	public void pintar() {
		System.out.println("Pintando una silla victoriana");		
		
	}

}
