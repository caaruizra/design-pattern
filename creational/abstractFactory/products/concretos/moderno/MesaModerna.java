package abstractFactory.products.concretos.moderno;

import abstractFactory.products.Mesa;

public class MesaModerna extends Mesa {
	

	@Override
	public void plegar() {
			System.out.println("Plegando una mesa moderna");		
	}

}
