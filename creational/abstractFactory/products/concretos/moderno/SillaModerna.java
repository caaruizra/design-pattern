package abstractFactory.products.concretos.moderno;

import abstractFactory.products.Silla;

public class SillaModerna extends Silla {

	@Override
	public void pintar() {
		System.out.println("Pintando una silla moderna");		

	}



}
