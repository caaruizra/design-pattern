package abstractFactory.products;


public abstract class Silla {
	private int numeroPatas;
	private int material;
	public int getNumeroPatas() {
		return numeroPatas;
	}
	public void setNumeroPatas(int numeroPatas) {
		this.numeroPatas = numeroPatas;
	}
	public int getMaterial() {
		return material;
	}
	public void setMaterial(int material) {
		this.material = material;
	}
	
	public abstract void pintar();

}
