package abstractFactory;

import abstractFactory.factory.AbstractFactory;
import abstractFactory.factory.victoriana.VictorianaFactory;

public class Main {

	public static void main(String[] args) {
		AbstractFactory mf = new VictorianaFactory();
		Client c = new Client(mf);
		c.doOperation();
	}

}
