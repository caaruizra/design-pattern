package abstractFactory;

import abstractFactory.factory.AbstractFactory;

public class Client {
	private AbstractFactory factory ;
	
	Client(AbstractFactory factory){
		this.factory = factory;
	}
	
	public void doOperation() { 
		this.factory.createMesa().plegar();
		this.factory.createSilla().pintar();
	}
	
	
	
	
}
