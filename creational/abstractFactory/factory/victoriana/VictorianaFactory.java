package abstractFactory.factory.victoriana;

import abstractFactory.factory.AbstractFactory;
import abstractFactory.products.Mesa;
import abstractFactory.products.Silla;
import abstractFactory.products.concretos.victoriana.MesaVictoriana;
import abstractFactory.products.concretos.victoriana.SillaVictoriana;

public class VictorianaFactory implements AbstractFactory {

	@Override
	public Mesa createMesa() {
		return new MesaVictoriana();
	}

	@Override
	public Silla createSilla() {
		return new SillaVictoriana();
	}
	
}
