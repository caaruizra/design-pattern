package abstractFactory.factory.moderna;

import abstractFactory.factory.AbstractFactory;
import abstractFactory.products.Mesa;
import abstractFactory.products.Silla;
import abstractFactory.products.concretos.moderno.MesaModerna;
import abstractFactory.products.concretos.moderno.SillaModerna;

public class ModernaFactory implements AbstractFactory {

	@Override
	public Mesa createMesa() {
		return new MesaModerna();
	}

	@Override
	public Silla createSilla() {
		return new SillaModerna();
	}
	
}
