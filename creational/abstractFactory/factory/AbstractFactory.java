package abstractFactory.factory;

import abstractFactory.products.Mesa;
import abstractFactory.products.Silla;

public interface AbstractFactory {
	public Mesa createMesa();
	public Silla createSilla();
}
