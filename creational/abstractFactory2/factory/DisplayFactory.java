package abstractFactory2.factory;

import abstractFactory2.products.Button;
import abstractFactory2.products.Dialog;
import abstractFactory2.products.ScrollBar;

public interface DisplayFactory {
	public Button createButton();
	public Dialog createDialog();
	public ScrollBar createScrollBar();
}
