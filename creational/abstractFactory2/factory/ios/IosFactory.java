package abstractFactory2.factory.ios;

import abstractFactory2.factory.DisplayFactory;
import abstractFactory2.products.Button;
import abstractFactory2.products.Dialog;
import abstractFactory2.products.ScrollBar;
import abstractFactory2.products.concretos.ios.IosButton;
import abstractFactory2.products.concretos.ios.IosDialog;
import abstractFactory2.products.concretos.ios.IosScrollBar;

public class IosFactory implements DisplayFactory {

	@Override
	public Button createButton() {
		return new IosButton();
	}

	@Override
	public Dialog createDialog() {
		return new IosDialog();
	}

	@Override
	public ScrollBar createScrollBar() {
		return new IosScrollBar();
	}

	
	
}
