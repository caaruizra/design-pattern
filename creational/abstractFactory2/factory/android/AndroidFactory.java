package abstractFactory2.factory.android;

import abstractFactory2.factory.DisplayFactory;
import abstractFactory2.products.Button;
import abstractFactory2.products.Dialog;
import abstractFactory2.products.ScrollBar;
import abstractFactory2.products.concretos.android.AndroidButton;
import abstractFactory2.products.concretos.android.AndroidDialog;
import abstractFactory2.products.concretos.android.AndroidScrollBar;

public class AndroidFactory implements DisplayFactory {
	@Override
	public Button createButton() {
		return new AndroidButton();
	}

	@Override
	public Dialog createDialog() {
		return new AndroidDialog();
	}

	@Override
	public ScrollBar createScrollBar() {
		return new AndroidScrollBar();
	}
	
}
