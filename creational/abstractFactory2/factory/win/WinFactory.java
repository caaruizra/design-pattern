package abstractFactory2.factory.win;

import abstractFactory2.factory.DisplayFactory;
import abstractFactory2.products.Button;
import abstractFactory2.products.Dialog;
import abstractFactory2.products.ScrollBar;
import abstractFactory2.products.concretos.ios.IosButton;
import abstractFactory2.products.concretos.ios.IosDialog;
import abstractFactory2.products.concretos.ios.IosScrollBar;
import abstractFactory2.products.concretos.win.WinButton;
import abstractFactory2.products.concretos.win.WinDialog;
import abstractFactory2.products.concretos.win.WinScrollBar;

public class WinFactory implements DisplayFactory {

	@Override
	public Button createButton() {
		return new WinButton();
	}

	@Override
	public Dialog createDialog() {
		return new WinDialog();
	}

	@Override
	public ScrollBar createScrollBar() {
		return new WinScrollBar();
	}

	
	
}
