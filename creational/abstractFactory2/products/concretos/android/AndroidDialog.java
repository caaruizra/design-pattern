package abstractFactory2.products.concretos.android;

import abstractFactory2.products.Dialog;

public class AndroidDialog extends Dialog {

	@Override
	public void draw() {
		System.out.println("Drawing a Android dialog");
	}

}
