package abstractFactory2.products.concretos.android;

import abstractFactory2.products.Button;

public class AndroidButton extends Button {

	@Override
	public void draw() {
		System.out.println("Drawing a Android button");

	}

}
