package abstractFactory2.products.concretos.android;

import abstractFactory2.products.ScrollBar;

public class AndroidScrollBar extends ScrollBar {

	@Override
	public void draw() {
		System.out.println("Drawing a Android ScrollBar");
	}

}
