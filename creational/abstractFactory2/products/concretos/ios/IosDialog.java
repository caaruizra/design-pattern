package abstractFactory2.products.concretos.ios;

import abstractFactory2.products.Dialog;

public class IosDialog extends Dialog {

	@Override
	public void draw() {
		System.out.println("Drawing a IOS dialog");
	}

}
