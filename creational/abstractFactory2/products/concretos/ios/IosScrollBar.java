package abstractFactory2.products.concretos.ios;

import abstractFactory2.products.ScrollBar;

public class IosScrollBar extends ScrollBar {

	@Override
	public void draw() {
		System.out.println("Drawing a IOS ScrollBar");
	}

}
