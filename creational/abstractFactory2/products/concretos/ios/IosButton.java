package abstractFactory2.products.concretos.ios;

import abstractFactory2.products.Button;

public class IosButton extends Button {

	@Override
	public void draw() {
		System.out.println("Drawing a IOS button");

	}

}
