package abstractFactory2.products.concretos.win;

import abstractFactory2.products.Dialog;

public class WinDialog extends Dialog {

	@Override
	public void draw() {
		System.out.println("Drawing a Windows dialog");
	}

}
