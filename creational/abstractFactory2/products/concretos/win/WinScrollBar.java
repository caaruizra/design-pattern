package abstractFactory2.products.concretos.win;

import abstractFactory2.products.ScrollBar;

public class WinScrollBar extends ScrollBar {

	@Override
	public void draw() {
		System.out.println("Drawing a Windows ScrollBar");
	}

}
