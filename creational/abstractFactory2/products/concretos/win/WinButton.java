package abstractFactory2.products.concretos.win;

import abstractFactory2.products.Button;

public class WinButton extends Button {

	@Override
	public void draw() {
		System.out.println("Drawing a windows button");

	}

}
