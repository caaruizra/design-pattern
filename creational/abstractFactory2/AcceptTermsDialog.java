package abstractFactory2;

import abstractFactory2.factory.DisplayFactory;

public class AcceptTermsDialog {
	private DisplayFactory factory ;
	
	AcceptTermsDialog(DisplayFactory factory){
		this.factory = factory;
	}
	
	public void doOperation() { 
		this.factory.createButton().draw();
		this.factory.createDialog().draw();
		this.factory.createScrollBar().draw();
	}
	
	
	
	
}
