package abstractFactory2;

import abstractFactory2.factory.DisplayFactory;
import abstractFactory2.factory.android.AndroidFactory;
import abstractFactory2.factory.ios.IosFactory;
import abstractFactory2.factory.win.WinFactory;

public class Main {

	public static void main(String[] args) {
		DisplayFactory displayFactory;
		//String os = "Win";
		//String os = "IOS";
		String os = "Android";
		
		if(os.equals("IOS")) {
			displayFactory = new IosFactory();
		}else if(os.equals("Win")) {
			displayFactory = new WinFactory();
		}else { 
			displayFactory = new AndroidFactory();
		}
		
		AcceptTermsDialog c = new AcceptTermsDialog(displayFactory);
		c.doOperation();
	}

}
