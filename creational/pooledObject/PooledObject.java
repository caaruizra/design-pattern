package pooledObject;

class PooledObject {

	public PooledObject() {
		try {
			System.out.println("Iniciando PooledObject: " + hashCode());
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	public void executeStament(String id) {
		System.out.println("Executing statment " +  hashCode());
	}

}