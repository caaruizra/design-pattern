package pooledObject;

public class Main {
	  public static void main(String[] args) {
	        ObjectPool objectPool = new ObjectPool(2);
	       
	        PooledObject obj1 = objectPool.acquireObject();
	        PooledObject obj2 = objectPool.acquireObject();
	        
	        obj1.executeStament("obj1");
	        obj2.executeStament("obj2");
	        	        
	        objectPool.releaseObject(obj1);
	        objectPool.releaseObject(obj2);
	        
	        System.out.println("Ahora vamos a adquirir nuevos objetos");
	        PooledObject obj3 = objectPool.acquireObject();
	        PooledObject obj4 = objectPool.acquireObject();
	        PooledObject obj5 = objectPool.acquireObject();

	        obj3.executeStament("obj3");
	        obj4.executeStament("obj4");
	        
	        objectPool.releaseObject(obj3);
	        objectPool.releaseObject(obj4);
	        objectPool.releaseObject(obj5);

	        
	        obj3 = objectPool.acquireObject();
	        obj4 = objectPool.acquireObject();
	        obj5 = objectPool.acquireObject();

	        
	        
	    }

}
