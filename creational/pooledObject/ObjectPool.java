package pooledObject;
import java.util.ArrayList;
import java.util.List;

class ObjectPool {
    private List<PooledObject> pool;
    private int maxSize;
    
    public ObjectPool(int maxSize) {
        this.maxSize = maxSize;
        this.pool = new ArrayList<>();
    }
    
    public synchronized PooledObject acquireObject() {
        if (pool.isEmpty()) {
            return new PooledObject();
        } else {
            return pool.remove(pool.size() - 1);
        }
    }
    
    public synchronized void releaseObject(PooledObject object) {
        if (pool.size() < maxSize) {
            pool.add(object);
        }
    }
    
    
}
