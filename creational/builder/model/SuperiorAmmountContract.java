package builder.model;

import java.security.Provider;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class SuperiorAmmountContract {
	Employee supervisor;
	Employee responsible;
	Provider company;
	Date publishDate;
	Date endDate;
	String profile;
	List<String> specificObligations ;
	List<CDP> cdps ;
	double ammount;
	String id;
	ContractTypes type;

	
	
	
	public SuperiorAmmountContract(Employee supervisor, Employee responsible, Provider company, Date publishDate,
			Date endDate, String profile, List<String> specificObligations, List<CDP> cdps, double ammount, ContractTypes type) {
		super();
		this.supervisor = supervisor;
		this.responsible = responsible;
		this.company = company;
		this.publishDate = publishDate;
		this.endDate = endDate;
		this.profile = profile;
		this.specificObligations = specificObligations;
		this.cdps = cdps;
		this.ammount = ammount;
		
		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM");
		Date date = new Date(System.currentTimeMillis());
		this.type =type;
		
		this.id = formatter.format(date) + (Math.random()*1000);
	}


	@Override
	public String toString() {
		return "SuperiorAmmountContract [supervisor=" + supervisor + ", responsible=" + responsible + ", company="
				+ company + ", publishDate=" + publishDate + ", endDate=" + endDate + ", profile=" + profile
				+ ", specificObligations=" + specificObligations + ", cdps=" + cdps + ", ammount=" + ammount + "]";
	}
	
	

}
