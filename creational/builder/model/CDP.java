package builder.model;

import java.util.Date;

public class CDP {
	private Date createDate;
	private String ID;
	private String object;
	
	
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getObject() {
		return object;
	}
	public void setObject(String object) {
		this.object = object;
	}
	public CDP(Date createDate, String iD, String object) {
		super();
		this.createDate = createDate;
		ID = iD;
		this.object = object;
	}
	
}
