package builder.builder;

import java.security.Provider;
import java.util.Date;

import builder.model.CDP;
import builder.model.ContractTypes;
import builder.model.Employee;

public interface BuilderContract {
	
	public BuilderContract setSupervisor(Employee supervisor);
	public BuilderContract addProvider(Provider company);
	public BuilderContract setEndDate(Date date);
	public BuilderContract setPublishDate(Date date);
	public BuilderContract setProfile(String profile);
	public BuilderContract addSpecificObligation(String obligation);
	public BuilderContract addCDP(CDP cdp);
	public BuilderContract setAmount(double ammount);
	
	public BuilderContract setResponsible(Employee responsible);
	
	public BuilderContract setType(ContractTypes types);

	

}
