package builder.builder;

import java.security.Provider;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import builder.model.CDP;
import builder.model.ContractTypes;
import builder.model.Employee;
import builder.model.MinimumAmmountContract;

public class MinimumAmmountContractBuilder implements BuilderContract {

	Employee supervisor;
	Employee responsible;
	Provider company;
	Date publishDate;
	Date endDate;
	String profile;
	List<String> specificObligations;
	List<CDP> cdps;
	double ammount;
	ContractTypes type;

	
	

	public MinimumAmmountContractBuilder() {
		this.specificObligations = new ArrayList<>();
		this.cdps = new ArrayList<>();
	}

	@Override
	public BuilderContract setSupervisor(Employee supervisor) {
		this.supervisor = supervisor;
		return this;
	}

	@Override
	public BuilderContract addProvider(Provider company) {
		this.company = company;
		return this;
	}

	@Override
	public BuilderContract setEndDate(Date date) {
		this.endDate = date;
		return this;
	}

	@Override
	public BuilderContract setPublishDate(Date date) {
		this.publishDate = date;
		return this;
	}

	@Override
	public BuilderContract setProfile(String profile) {
		this.profile = profile;
		return this;
	}

	@Override
	public BuilderContract addSpecificObligation(String obligation) {
		this.specificObligations.add(obligation);
		return this;
	}

	@Override
	public BuilderContract addCDP(CDP cdp) {
		this.cdps.add(cdp);
		return this;
	}

	@Override
	public BuilderContract setAmount(double ammount) {
		this.ammount = ammount;
		return this;
	}

	@Override
	public BuilderContract setResponsible(Employee responsible) {
		this.responsible = responsible;
		return this;
	}

	public MinimumAmmountContract getResult() {
		return new MinimumAmmountContract(supervisor, responsible, company, publishDate, endDate, profile,
				specificObligations, cdps, ammount,type);
	}

	@Override
	public BuilderContract setType(ContractTypes types) {
		this.type = types;
		return this;
	}

}
