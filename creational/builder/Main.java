package builder;


import builder.builder.MinimumAmmountContractBuilder;
import builder.builder.SuperiorAmmountContractBuilder;
import builder.director.Director;
import builder.model.MinimumAmmountContract;
import builder.model.SuperiorAmmountContract;

public class Main {
	public static void main(String[] arg) {
		
		Director d = new Director();
		
		MinimumAmmountContractBuilder minimumBuilder = new MinimumAmmountContractBuilder();
		d.constructOps(minimumBuilder);
		MinimumAmmountContract minimalContract = minimumBuilder.getResult();
		System.out.println(minimalContract);
		
		
		SuperiorAmmountContractBuilder superiorBuilder = new SuperiorAmmountContractBuilder();
		d.constructOse(superiorBuilder);
		SuperiorAmmountContract superiorContract = superiorBuilder.getResult();
		System.out.println(superiorContract);
		
		
		
		
	}
}
