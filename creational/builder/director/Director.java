package builder.director;

import java.util.Date;

import builder.builder.BuilderContract;
import builder.model.CDP;
import builder.model.ContractTypes;
import builder.model.Employee;

public class Director {

	public BuilderContract constructOse(BuilderContract builder) {
		builder.setAmount(1000).setEndDate(new Date(1685776825)).setPublishDate(new Date(System.currentTimeMillis()))
				.setSupervisor(new Employee("Juan", "Perez", "13456")).setType(ContractTypes.OSE);
		return builder;

	}

	public BuilderContract constructOps(BuilderContract builder) {
	        builder.setAmount(1000);
	        builder.setEndDate(new Date(1685776825));
	        builder.setPublishDate(new Date(System.currentTimeMillis()));
	        builder.setSupervisor(new Employee("Juan","Perez","13456"));
	        builder.addCDP(new CDP(new Date(System.currentTimeMillis()),   "3010-2020-52", "1500"));
	        builder.setType(ContractTypes.OPS);
	        return builder;
	    }

}
