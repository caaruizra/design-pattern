package multiton;
import java.util.HashMap;
import java.util.Map;

public class Multiton {
	 private static Map<String, Multiton> instances = new HashMap<>();
	   
	 
	 	private String name;
	    
	    private Multiton(String name) {
	        this.name = name;
	    }
	    
	    public static synchronized Multiton getInstance(String name) {
	        if (!instances.containsKey(name)) {
	            Multiton instance = new Multiton(name);
	            instances.put(name, instance);
	        }
	        return instances.get(name);
	    }

		public static Map<String, Multiton> getInstances() {
			return instances;
		}

		public static void setInstances(Map<String, Multiton> instances) {
			Multiton.instances = instances;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	    
	    
}
