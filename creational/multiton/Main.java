package multiton;
public class Main {
	public static void main(String[] args) {
		Multiton instance1 = Multiton.getInstance("Instance 1");
		Multiton instance2 = Multiton.getInstance("Instance 2");
		Multiton instance3 = Multiton.getInstance("Instance 1"); // Obtiene la misma instancia que instance1

		System.out.println(instance1 == instance2); // false
		System.out.println(instance1 == instance3); // true
	}
}