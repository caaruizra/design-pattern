package factory2;

import factory2.products.ImageFile;

public abstract class ImageHandler {
	private String type;
	
	public ImageHandler(String type) {
		this.type = type;
	}
	
	public abstract ImageFile createImage(String type) ;
	
	public void processImage() {
		ImageFile image = this.createImage(this.type);
		image.openImage();
		image.resize(200, 300);
	}

}
