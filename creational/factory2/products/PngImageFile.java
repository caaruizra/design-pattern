package factory2.products;

public class PngImageFile implements ImageFile {

	@Override
	public void openImage() {
		System.out.println("Imagen se muestra archivo PNG");
	}

	@Override
	public void resize(int width, int height) {
		System.out.println("Imagen se redimensiona a "+width+"X"+ height);
	}

}
