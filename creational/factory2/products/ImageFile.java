package factory2.products;

public interface ImageFile {
	public void openImage();
	public void resize(int width, int height);
	
}
