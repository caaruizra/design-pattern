package factory2;

import factory2.products.ImageFile;
import factory2.products.JpgImageFile;
import factory2.products.PngImageFile;

public class ImageHandlerImpl extends ImageHandler {
	public ImageHandlerImpl(String type) {
		super(type);
	}

	@Override
	public ImageFile createImage(String type) {
		if (type.equalsIgnoreCase("Jpg")) {
			return new JpgImageFile();
		} else if (type.equalsIgnoreCase("png")) {
			return new PngImageFile();
		} else {
			return null;
		}
	}

}
