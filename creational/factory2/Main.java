package factory2;


public class Main {
	
	static ImageHandler handler;
	
	public static void configure(String ext){
			handler = new ImageHandlerImpl(ext);
	}
	
	public static void main(String argsv[]) {
		 configure("jpg");
		 handler.processImage();
		 
		 configure("png");
		 handler.processImage();

		
	}

}
