package singleton;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Main {
	public static void main(String [] argsv) throws SQLException {
		DBConnectWrapper c = DBConnectWrapper.getInstance();
		Statement stm = c.connection.createStatement();
		ResultSet rs = stm.executeQuery("SELECT 40 FROM dual"); 
		
		while(rs.next())  
			System.out.println(rs.getString(1));  
		
		c.getConnection().close();
		
		c = DBConnectWrapper.getInstance();
		stm = c.connection.createStatement();
		rs = stm.executeQuery("SELECT * FROM dual"); 
		while(rs.next())  
			System.out.println(rs.getString(1));  
	}
}
