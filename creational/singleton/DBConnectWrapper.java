package singleton;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnectWrapper {
	
	private static DBConnectWrapper dbConnection;
	Connection connection;
		  
	private DBConnectWrapper() {
		try{  
			System.out.println("Conectando a oracle");

			Class.forName("oracle.jdbc.driver.OracleDriver");  
			connection=DriverManager.getConnection( 
			"jdbc:oracle:thin:@localhost:1521:xe","alejandro","abcd1234");  
			
			System.out.println("conectado");

		}catch(Exception e) {
			System.out.println("Error de conexiòn");
		}
	}
	
	
	public static DBConnectWrapper getInstance() { 
		if(dbConnection == null)
			dbConnection = new DBConnectWrapper();
		return dbConnection;
	}
	 public Connection getConnection() {
		 return this.connection;
	 }
	

}
