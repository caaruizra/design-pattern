package prototype.prototype;

public  class Violin extends MusicalInstrument {
	private int age;
	
	public Violin() {
	}
	
	public Violin(Violin target) {
		super(target);
		this.age = target.age;
	}
	
	

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public Violin clone() {
		return new Violin(this);
	}

	@Override
	public String toString() {
		return "Violin age=" + age + " name=" +this.getName() + " price="+this.getPrice()+" description="+this.getDescription();
	}
	
	

}
