package prototype.prototype;

public  class Guitar extends MusicalInstrument {
	private int numberOflStrings;
	
	public Guitar() {
	}
	
	public Guitar(Guitar target) {
		super(target);
		this.numberOflStrings = target.numberOflStrings;
	}
	
	
	
	public int getNumberStrings() {
		return numberOflStrings;
	}

	public void setNumberStrings(int numberStrings) {
		this.numberOflStrings = numberStrings;
	}

	@Override
	public Guitar clone() {
		return new Guitar(this);
	}

	@Override
	public String toString() {
		return "Guitar numberOflStrings=" + numberOflStrings + " name=" +this.getName() + " price="+this.getPrice()+" description="+this.getDescription();
	}
	
	

}
