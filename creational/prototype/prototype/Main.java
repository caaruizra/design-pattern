package prototype.prototype;

public class Main {

	public static void main(String[] args) {
		MusicalInstrument guitar = new Guitar();
		guitar.setName("Fender");
		guitar.setDescription("American Deluxe");
		guitar.setPrice(1500);
		
		System.out.println("-- Before --");
		System.out.println(guitar);
		
		
		System.out.println("-- After --");
		MusicalInstrument guitar2 =  guitar.clone();
		System.out.println(guitar);
		System.out.println(guitar2);
		
		System.out.println("-- Modified --");
		guitar.setDescription("American Deluxe Special");
		System.out.println(guitar);
		System.out.println(guitar2);
		
		
		
		

	}

}
