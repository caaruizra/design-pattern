package prototype.prototype;

public abstract class MusicalInstrument {
	private double price;
	private String name;
	private String  description;
	
	public MusicalInstrument() {
	}
	
	public MusicalInstrument(MusicalInstrument musicalInstrument) {
		this.price = musicalInstrument.price;
		this.name = musicalInstrument.name;
		this.description = musicalInstrument.description;
	}
	
	public double getPrice() {
		return price;
	}
	
	public String  getName() {
		return name;
	}

	public void setName(String  name) {
		this.name = name;
	}

	public String  getDescription() {
		return description;
	}

	public void setDescription(String  description) {
		this.description = description;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public abstract MusicalInstrument clone();
	

}
