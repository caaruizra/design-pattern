package prototype.registry;
import java.util.HashMap;
import java.util.Map;

import prototype.prototype.Guitar;
import prototype.prototype.MusicalInstrument;

public class InstrumentCache {
 
	private Map<String, MusicalInstrument> cache = new HashMap<>();

    public InstrumentCache() {
      MusicalInstrument g1 = new Guitar();
      g1.setDescription("American Deluxe");
      g1.setName("Fender");
      g1.setPrice(150);
      
      cache.put("fender-deluxe", g1);
      
      
      MusicalInstrument g2 = new Guitar();
      g2.setDescription("American Standard");
      g2.setName("Fender");
      g2.setPrice(150);
      cache.put("fender-std", g2);
    }
    
    public void put(String key, MusicalInstrument inst) {
        cache.put(key, inst);
    }
    
    public MusicalInstrument get(String key) {
        return cache.get(key).clone();
    }
    
}
